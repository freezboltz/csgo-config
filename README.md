# csgo-config

Config file for counter strike global offensive
- custom key bindings
- configuration

## Installation
```sh
git clone "https://gitlab.com/freezboltz/csgo-config.git" && cd csgo-config
```
```sh
cp autoexec.cfg config.cfg $HOME/.local/share/Steam/steamapps/common/Counter-Strike Global Offensive/csgo/cfg
```
Make `config.cfg` file readonly
```sh
cd $HOME/.local/share/Steam/steamapps/common/Counter-Strike Global Offensive/csgo/cfg
chmod 444 config.cfg
```
